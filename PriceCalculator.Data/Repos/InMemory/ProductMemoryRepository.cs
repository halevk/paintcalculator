﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.InMemory
{
    public class ProductMemoryRepository : IProductRepository
    {
        private readonly MemoryDB _memoryDb;

        public ProductMemoryRepository(MemoryDB memoryDb)
        {
            _memoryDb = memoryDb;
        }

        public Task<Product[]> GetProductsBySectorIds(int[] sectorIds, CancellationToken token = default)
        {
            var productSectors = _memoryDb.SectorProducts.Where(p => sectorIds.Contains(p.SectorId))
                .GroupBy(p => p.ProductId);
            var products = _memoryDb.Products.Where(p => productSectors.Select(o => o.Key).Contains(p.Id));
            foreach (var product in products)
            {
                var sectorProduct = productSectors.FirstOrDefault(p => p.Key == product.Id);
                if(sectorProduct==null) continue;
                product.SectorProducts = sectorProduct.ToList();   
            }
            return Task.FromResult(products.ToArray());
        }
    }
}