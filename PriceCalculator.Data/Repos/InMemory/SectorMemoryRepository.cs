﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.InMemory
{
    public class SectorMemoryRepository : ISectorRepository
    {
        private readonly MemoryDB _memoryDb;

        public SectorMemoryRepository(MemoryDB memoryDb)
        {
            _memoryDb = memoryDb;
        }

        public Task<Sector[]> GetSectorsByProjectType(ProjectType[] types, CancellationToken token = default)
        {
            var projectTypes = _memoryDb.SectorProjectTypes.Where(p => types.Contains(p.ProjectType))
                .GroupBy(p => p.SectorId);
            var sectors = _memoryDb.Sectors.Where(p => projectTypes.Select(o => o.Key).Contains(p.Id));
            foreach (var sector in sectors)
            {
                var sectorType = projectTypes.FirstOrDefault(p => p.Key == sector.Id);
                if(sectorType==null) continue;
                sector.SectorProjectTypes = sectorType.ToList();
            }
            return Task.FromResult(sectors.ToArray());
        }
    }
}