﻿using System.Collections.Generic;
using PriceCalculator.Data.Entities;
using Color = PriceCalculator.Data.Entities.Color;

namespace PriceCalculator.Data.Repos.InMemory
{
    public class MemoryDB
    {
        public  List<Color> Colors => new List<Color>
        {
            new Color { Id = 1, Name = "Just White", HexCode = "#FFFFFF" },
            new Color { Id = 2, Name = "Elephant's breath", HexCode = "#d0c2b4" },
            new Color { Id = 3, Name = "Crown", HexCode = "#d64b30" },
            new Color { Id = 4, Name = "Julie's Dream", HexCode = "#efe3d8" },
            new Color { Id = 5, Name = "Dead Salmon", HexCode = "#c29e8f" },
            new Color { Id = 6, Name = "Nacho Cheese", HexCode = "#ffc56d" }
        };

        public  List<Sector> Sectors => new List<Sector>
        {
            new Sector{Id = 1,Name = "Hospital",CostMultiplier = 1.1m},
            new Sector{Id = 2,Name = "School",CostMultiplier = 1.0m},
            new Sector{Id = 3,Name = "Housing",CostMultiplier = 0.9m},
            new Sector{Id = 4,Name = "Hall",CostMultiplier = 1.0m},
            new Sector{Id = 5,Name = "Park",CostMultiplier = 0.8m}
        };

        public  List<SectorProjectType> SectorProjectTypes => new List<SectorProjectType>
        {
            new SectorProjectType { SectorId = 1, ProjectType = ProjectType.Interior },
            new SectorProjectType { SectorId = 1, ProjectType = ProjectType.Exterior },
            new SectorProjectType { SectorId = 2, ProjectType = ProjectType.Interior },
            new SectorProjectType { SectorId = 2, ProjectType = ProjectType.Exterior },
            new SectorProjectType { SectorId = 3, ProjectType = ProjectType.Interior },
            new SectorProjectType { SectorId = 3, ProjectType = ProjectType.Exterior },
            new SectorProjectType { SectorId = 4, ProjectType = ProjectType.Interior },
            new SectorProjectType { SectorId = 5, ProjectType = ProjectType.Exterior }
        };

        public  List<Product> Products => new List<Product>
        {
            new Product{Id = 1, Name = "Glidden Pro", Price = 0.4m, RedecorationCycle = 5},
            new Product{Id = 2, Name = "Mustang Speedwall", Price = 0.1m, RedecorationCycle = 1},
            new Product{Id = 3, Name = "Lifemaster Accents", Price = 0.8m, RedecorationCycle = 9},
            new Product{Id = 4, Name = "Weatherward Max", Price = 0.6m, RedecorationCycle = 3},
            new Product{Id = 5, Name = "Acrylic Diamond", Price = 0.8m, RedecorationCycle = 8}
        };

        public  List<SectorProduct> SectorProducts => new List<SectorProduct>
        {
            new SectorProduct { SectorId = 1, ProductId = 1 },
            new SectorProduct { SectorId = 2, ProductId = 1 },
            new SectorProduct { SectorId = 4, ProductId = 1 },
            new SectorProduct { SectorId = 2, ProductId = 2 },
            new SectorProduct { SectorId = 3, ProductId = 2 },
            new SectorProduct { SectorId = 5, ProductId = 2 },
            new SectorProduct { SectorId = 1, ProductId = 3 },
            new SectorProduct { SectorId = 3, ProductId = 4 },
            new SectorProduct { SectorId = 4, ProductId = 4 },
            new SectorProduct { SectorId = 5, ProductId = 4 },
            new SectorProduct { SectorId = 1, ProductId = 5 },
            new SectorProduct { SectorId = 2, ProductId = 5 },
            new SectorProduct { SectorId = 3, ProductId = 5 },
            new SectorProduct { SectorId = 4, ProductId = 5 },
            new SectorProduct { SectorId = 5, ProductId = 5 }
        };
    }
}