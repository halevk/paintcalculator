﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.InMemory
{
    public class SectorProductMemoryRepository : ISectorProductRepository
    {
        private readonly MemoryDB _memoryDb;

        public SectorProductMemoryRepository(MemoryDB memoryDb)
        {
            _memoryDb = memoryDb;
        }

        public Task<SectorProduct> GetBySectorAndProductId(int sectorId, int productId, CancellationToken token = default)
        {
            var query = _memoryDb.SectorProducts
                .Join(_memoryDb.Products, sp => sp.ProductId, p => p.Id, (sp, p) => new { sp, p })
                .Join(_memoryDb.Sectors, @t => t.sp.SectorId, s => s.Id, (@t, s) => new { t.sp, s, t.p })
                .Where(p => p.sp.SectorId == sectorId && p.sp.ProductId == productId)
                .Select(o =>
                {
                    o.sp.Product = o.p;
                    o.sp.Sector = o.s;
                    return o.sp;
                });
            return Task.FromResult(query.FirstOrDefault());
        }
    }
}