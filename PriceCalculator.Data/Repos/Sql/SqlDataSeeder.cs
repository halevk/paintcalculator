﻿using System.Data;
using System.Threading.Tasks;
using Dapper;

namespace PriceCalculator.Data.Repos.Sql
{
    public class SqlDataSeeder
    {
        private readonly IDbConnection _connection;

        public SqlDataSeeder(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task Seed()
        {
            using (_connection)
            {
                if(_connection.State==ConnectionState.Closed)
                    _connection.Open();
                using (var trans = _connection.BeginTransaction())
                {
                    await _connection.ExecuteAsync(@"
DROP DATABASE PaintDB;
CREATE DATABASE PaintDB COLLATE Latin1_General_CI_AI");
                    await _connection.ExecuteAsync(SeedData);
                    trans.Commit();
                }
            }
        }

        private static string SeedData =>
            @"
            DROP TABLE IF EXISTS dbo.Colors;
CREATE TABLE dbo.Colors(
Id int NOT NULL PRIMARY KEY IDENTITY(1,1),
Name nvarchar(50) NOT NULL,
HexCode nvarchar(7) NOT NULL);

INSERT INTO dbo.Colors VALUES
   ('Just White','#FFFFFF'),
   ('Elephant''s breath','#d0c2b4'),
   ('Crown','#d64b30'),
   ('Julie''s Dream','#efe3d8'),
   ('Dead Salmon','#c29e8f'),
   ('Nacho Cheese','#ffc56d')


DROP TABLE IF EXISTS dbo.Products;
CREATE TABLE dbo.Products(
    Id int NOT NULL PRIMARY KEY IDENTITY(1,1),
    Name varchar(50) NOT NULL,
    Price decimal(3,2) NOT NULL,
    RedecorationCycle int NOT NULL);

INSERT INTO dbo.Products VALUES
   ('Glidden Pro',0.4,5),
   ('Mustang Speedwall',0.1,1),
   ('Lifemaster Accents',0.8,9),
   ('Weatherward Max',0.6,3),
   ('Acrylic Diamond',0.8,8)


DROP TABLE IF EXISTS dbo.Sectors;
CREATE TABLE dbo.Sectors(
Id int NOT NULL PRIMARY KEY IDENTITY(1,1),
Name varchar(50),
CostMultiplier decimal(3,2));

INSERT INTO dbo.Sectors VALUES
  ('Hospital',1.1),
  ('School',1.0),
  ('Housing',0.9),
  ('Hall',1.0),
  ('Park',0.8)


DROP TABLE IF EXISTS dbo.SectorProjectTypes;
CREATE TABLE dbo.SectorProjectTypes (
SectorId int NOT NULL FOREIGN KEY REFERENCES Sectors(Id),
ProjectType int NOT NULL);

INSERT INTO dbo.SectorProjectTypes VALUES
  (1,0),
  (1,1),
  (2,0),
  (2,1),
  (3,0),
  (3,1),
  (4,0),
  (5,1)


DROP TABLE IF EXISTS dbo.SectorProducts;
CREATE TABLE dbo.SectorProducts(
    SectorId int NOT NULL FOREIGN KEY REFERENCES Sectors(Id),
    ProductId int NOT NULL FOREIGN KEY REFERENCES Products(Id));

INSERT INTO dbo.SectorProducts VALUES
   (1,1),
   (2,1),
   (4,1),
   (2,2),  
   (3,2),
   (5,2),
   (1,3),                                   
   (3,4),
   (4,4),
   (5,4),
   (1,5),
   (2,5),
   (3,5),
   (4,5),
   (5,5)
           ";
    }
}