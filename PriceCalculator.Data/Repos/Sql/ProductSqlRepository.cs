﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.Sql
{
    public class ProductSqlRepository : IProductRepository
    {
        private readonly IDbConnection _connection;

        public ProductSqlRepository(IDbConnection connection)
        {
            _connection = connection;
        }   

        public async Task<Product[]> GetProductsBySectorIds(int[] sectorIds, CancellationToken token = default)
        {
            using (_connection)
            {
                var sectorProducts = (await _connection.QueryAsync<SectorProduct>(
                        "SELECT * FROM SectorProducts WHERE SectorId In @SectorIds",
                        new { SectorIds = sectorIds }))
                    .GroupBy(p => p.ProductId);
                var products = await _connection.QueryAsync<Product>(
                    "SELECT * FROM Products WHERE Id IN @Ids", 
                    new { Ids = sectorProducts.Select(p => p.Key) });
                
                foreach (var product in products)
                {
                    var sectorProduct = sectorProducts.FirstOrDefault(p => p.Key == product.Id);
                    if(sectorProduct==null) continue;
                    product.SectorProducts = sectorProduct.ToList();
                }
                return products.ToArray();
            }
        }
    }
}