﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.Sql
{
    public class SectorSqlRepository : ISectorRepository
    {
        private readonly IDbConnection _connection;

        public SectorSqlRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task<Sector[]> GetSectorsByProjectType(ProjectType[] types, CancellationToken token = default)
        {
            using (_connection)
            {
                var projectTypes = (await _connection.QueryAsync<SectorProjectType>(
                        @"Select 
                    SectorId,
                    ProjectType 
                    FROM dbo.SectorProjectTypes 
                    WHERE ProjectType in @ProjectTypes",
                        new { ProjectTypes = types }))
                    .GroupBy(p => p.SectorId);
                
                var sectors = await _connection.QueryAsync<Sector>(
                    "Select * from Sectors where Id IN @Ids",
                    new { Ids = projectTypes.Select(o => o.Key) });
                foreach (var sector in sectors)
                {
                    var sectorType = projectTypes.FirstOrDefault(p => p.Key == sector.Id);
                    if(sectorType==null) continue;
                    sector.SectorProjectTypes = sectorType.ToList();
                }
                return sectors.ToArray();
            }
        }
    }
}