﻿using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Repos.Sql
{
    public class SectorProductSqlRepository : ISectorProductRepository
    {
        private readonly IDbConnection _connection;

        public SectorProductSqlRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task<SectorProduct> GetBySectorAndProductId(int sectorId, int productId, CancellationToken token = default)
        {
            using (_connection)
            {
                var sectorProduct = (await _connection.QueryAsync<SectorProduct, Product, Sector, SectorProduct>(
                        @"SELECT 
                    sp.*,
                    NULL as Part1,
                    p.*,
                    NULL as Part2,
                    s.*
                    FROM SectorProducts sp
                    INNER JOIN Products p ON p.Id=sp.ProductId
                    INNER JOIN Sectors s ON s.Id=sp.SectorId
                    WHERE sp.SectorId=@SectorId AND sp.ProductId=@ProductId",
                        (sp, p, s) =>
                        {
                            sp.Product = p;
                            sp.Sector = s;
                            return sp;
                        },
                        new { SectorId = sectorId, ProductId = productId },
                        splitOn: "Part1,Part2"))
                    .FirstOrDefault();
                return sectorProduct;
            }
        }
    }
}