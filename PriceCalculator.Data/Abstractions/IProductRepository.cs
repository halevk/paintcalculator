﻿using System;
using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Abstractions
{
    public interface IProductRepository
    {
        Task<Product[]> GetProductsBySectorIds(int[] sectorIds, CancellationToken token = default);
    }
}