﻿using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Abstractions
{
    public interface ISectorProductRepository
    {
        Task<SectorProduct> GetBySectorAndProductId(int sectorId, int productId, CancellationToken token = default);
    }
}