﻿using System.Threading;
using System.Threading.Tasks;
using PriceCalculator.Data.Entities;

namespace PriceCalculator.Data.Abstractions
{
    public interface ISectorRepository
    {
        Task<Sector[]> GetSectorsByProjectType(ProjectType[] types, CancellationToken token = default);
    }
}