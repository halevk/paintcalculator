﻿using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Repos.InMemory;
using PriceCalculator.Data.Repos.Sql;

namespace PriceCalculator.Data
{
    public static class RegisterServices
    {
        public static void AddData(this IServiceCollection services, IConfiguration config)
        {
            var url = config.GetValue<string>("ConnectionString");
            var useInMemoryDb = config.GetValue<bool>("UseInMemoryDb");
            if (useInMemoryDb)
            {
                services.AddSingleton<MemoryDB>();
                services.AddScoped<IProductRepository, ProductMemoryRepository>();
                services.AddScoped<ISectorRepository, SectorMemoryRepository>();
                services.AddScoped<ISectorProductRepository, SectorProductMemoryRepository>();
            }
            else
            {
                services.AddScoped<IDbConnection>(p => new SqlConnection(url));
                services.AddScoped<IProductRepository, ProductSqlRepository>();
                services.AddScoped<ISectorRepository, SectorSqlRepository>();
                services.AddScoped<ISectorProductRepository, SectorProductSqlRepository>();
            }
        }
    }
}