﻿namespace PriceCalculator.Data.Entities
{
    public enum ProjectType
    {
        Interior,
        Exterior
    }
}