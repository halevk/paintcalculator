﻿using System.Collections.Generic;

namespace PriceCalculator.Data.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int RedecorationCycle { get; set; }
        public List<SectorProduct> SectorProducts { get; set; }
    }

    public class Color
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HexCode { get; set; }
    }
}