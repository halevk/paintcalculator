﻿using System.Collections.Generic;

namespace PriceCalculator.Data.Entities
{
    public class Sector
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal CostMultiplier { get; set; }
        public List<SectorProjectType> SectorProjectTypes { get; set; }
        public List<SectorProduct> SectorProducts { get; set; }
    }

    public class SectorProduct
    {
        public int SectorId { get; set; }
        public Sector Sector { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }

    public class SectorProjectType
    {
        public int SectorId { get; set; }
        public ProjectType ProjectType { get; set; }
    }
}