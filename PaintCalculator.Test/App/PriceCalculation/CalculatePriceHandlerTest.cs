﻿using System.Threading.Tasks;
using PaintCalculator.App.UseCases.PriceCalculation.Command;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Repos.InMemory;
using Xunit;

namespace PaintCalculator.Test.App.PriceCalculation
{
    public class CalculatePriceHandlerTest
    {
        private readonly ISectorProductRepository _sectorProductRepository;

        public CalculatePriceHandlerTest()
        {
            _sectorProductRepository = new SectorProductMemoryRepository(new MemoryDB());
        }
        
        [Fact]
        public async Task Handle_Should_ReturnError_WhenSectorAndProductId_NotFound()
        {
            //Arrange
            var handler = new CalculatePriceHandler(_sectorProductRepository);

            //Act
            var response = await handler.Handle(new CalculatePriceRequest { ProductId = 1, SectorId = 7 }, default);

            //Assert
            Assert.True(response.HasError);
        }
        
        [Theory]
        [InlineData(3,1,1000,30,3520.00)]
        public async Task Handle_Should_CalculateTotalCostBy(int productId,int sectorId,double area,int period,double totalCost)
        {
            //Arrange
            var handler = new CalculatePriceHandler(_sectorProductRepository);

            //Act
            var response = await handler.Handle(new CalculatePriceRequest
            {
                ProductId = productId, 
                SectorId = sectorId,
                Area = area,
                Period = period,
                ColorName = "Crown"
            }, default);

            //Assert
            Assert.Empty(response.Errors);
            Assert.Equal(totalCost, response.TotalCost);
        }
    }
}