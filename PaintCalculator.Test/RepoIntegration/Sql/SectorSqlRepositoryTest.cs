﻿using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;
using PriceCalculator.Data.Repos.Sql;
using Xunit;

namespace PaintCalculator.Test.RepoIntegration.Sql
{
    public class SectorSqlRepositoryTest : TestBase
    {
        private readonly ISectorRepository _sectorRepository;

        public SectorSqlRepositoryTest()
        {
            _sectorRepository = new SectorSqlRepository(Connection);
        }

        [Fact]
        public async Task Should_GetSectorsBy_ProductTypes()
        {
            //Act
            var sectors = await _sectorRepository
                .GetSectorsByProjectType(new []
                {
                    ProjectType.Exterior
                });
            
            //Assert
            Assert.Equal(4, sectors.Length);
        }
    }
}