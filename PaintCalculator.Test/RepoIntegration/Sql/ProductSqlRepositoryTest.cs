﻿using System.Linq;
using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Repos.Sql;
using Xunit;

namespace PaintCalculator.Test.RepoIntegration.Sql
{
    public class ProductSqlRepositoryTest : TestBase
    {
        private readonly IProductRepository _productRepository;

        public ProductSqlRepositoryTest()
        {
            _productRepository = new ProductSqlRepository(Connection);
        }

        [Fact]
        public async Task Should_ReturnProductsBy_SectorIds()
        {
            //Act
            var sectorIds = new[] { 1, 2 };
            var products = await _productRepository.GetProductsBySectorIds(sectorIds);
            var distinctSectorIds = products.SelectMany(p => p.SectorProducts.Select(o => o.SectorId)).Distinct().ToArray();
            //Assert
            
            Assert.Equal(2,distinctSectorIds.Length);
            Assert.True(distinctSectorIds.All(p=> sectorIds.Contains(p)));
        }
    }
}