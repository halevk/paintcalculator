﻿using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Repos.Sql;
using Xunit;

namespace PaintCalculator.Test.RepoIntegration.Sql
{
    public class SectorProductSqlRepositoryTest : TestBase
    {
        private readonly ISectorProductRepository _sectorProductRepository;

        public SectorProductSqlRepositoryTest()
        {
            _sectorProductRepository = new SectorProductSqlRepository(Connection);
        }

        [Theory]
        [InlineData(1, 1)]
        public async Task Should_Return_SectorAnd_Product_ByGivenPair(int sectorId, int productId)
        {
            //Act
            var result = await _sectorProductRepository.GetBySectorAndProductId(sectorId, productId);
            
            //Assert
            Assert.Equal(result.Sector.Id,sectorId);
            Assert.Equal(result.Product.Id,productId);
        }
    }
}