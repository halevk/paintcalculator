﻿using System.Data;
using System.Data.SqlClient;

namespace PaintCalculator.Test.RepoIntegration.Sql
{
    public class TestBase
    {
        public IDbConnection Connection => new SqlConnection(
            "Server=tcp:localhost,1433;Initial Catalog=PaintDB;User ID=sa; Password=123456+-");

    }
}