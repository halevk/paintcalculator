﻿using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Repos.InMemory;
using Xunit;

namespace PaintCalculator.Test.RepoIntegration.InMemory
{
    public class SectorProductMemoryRepositoryTest
    {
        private readonly ISectorProductRepository _sectorProductRepository;

        public SectorProductMemoryRepositoryTest()
        {
            _sectorProductRepository = new SectorProductMemoryRepository(new MemoryDB());
        }
        
        [Theory]
        [InlineData(1, 1)]
        public async Task Should_Return_SectorAnd_Product_ByGivenPair(int sectorId, int productId)
        {
            //Act
            var result = await _sectorProductRepository.GetBySectorAndProductId(sectorId, productId);
            
            //Assert
            Assert.Equal(result.Sector.Id,sectorId);
            Assert.Equal(result.Product.Id,productId);
        }
    }
}