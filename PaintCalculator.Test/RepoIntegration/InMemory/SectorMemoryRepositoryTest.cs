﻿using System.Threading.Tasks;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;
using PriceCalculator.Data.Repos.InMemory;
using Xunit;

namespace PaintCalculator.Test.RepoIntegration.InMemory
{
    public class SectorMemoryRepositoryTest
    {
        private readonly ISectorRepository _sectorRepository;

        public SectorMemoryRepositoryTest()
        {
            _sectorRepository = new SectorMemoryRepository(new MemoryDB());
        }
        
        [Fact]
        public async Task Should_GetSectorsBy_ProductTypes()
        {
            //Act
            var sectors = await _sectorRepository
                .GetSectorsByProjectType(new []
                {
                    ProjectType.Exterior
                });
            
            //Assert
            Assert.Equal(4, sectors.Length);
        }
    }
}