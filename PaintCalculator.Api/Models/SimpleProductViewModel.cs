﻿namespace PaintCalculator.Api.Models
{
    public class SimpleProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int RedecorationCycle { get; set; }
    }
}