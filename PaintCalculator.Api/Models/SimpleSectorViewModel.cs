﻿namespace PaintCalculator.Api.Models
{
    public class SimpleSectorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal CostMultiplier { get; set; }
    }
}