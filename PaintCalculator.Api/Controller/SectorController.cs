﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaintCalculator.Api.Models;
using PriceCalculator.Data.Abstractions;
using PriceCalculator.Data.Entities;

namespace PaintCalculator.Api.Controller
{
    [ApiController]
    [Route("sector")]
    public class SectorController : ControllerBase
    {
        private readonly ISectorRepository _sectorRepository;

        public SectorController(ISectorRepository sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }
        
        [HttpGet,Route("{projectType}")]
        public async Task<IActionResult> SectorsByProjectType([FromRoute] string projectType)
        {
            if (!Enum.TryParse<ProjectType>(projectType, out var type))
                return BadRequest("project types could be Interior or Exterior");
            var sectors = await _sectorRepository.GetSectorsByProjectType(new[] { type });
            return Ok(sectors.Select(p => new SimpleSectorViewModel
                { Id = p.Id, Name = p.Name, CostMultiplier = p.CostMultiplier }));
        }
    }
}