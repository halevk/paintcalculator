﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PaintCalculator.App.UseCases.PriceCalculation.Command;

namespace PaintCalculator.Api.Controller
{
    [ApiController]
    [Route("calculate")]
    public class CalculateController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CalculateController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CalculatePrice([FromBody] CalculatePriceRequest request)
        {
            var totalPrice = await _mediator.Send(request);
            return Ok(new { TotalCost = totalPrice });
        }
    }
}