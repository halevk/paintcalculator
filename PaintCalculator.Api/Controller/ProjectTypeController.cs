﻿using System;
using Microsoft.AspNetCore.Mvc;
using PriceCalculator.Data.Entities;

namespace PaintCalculator.Api.Controller
{
    [ApiController]
    [Route("project-types")]
    public class ProjectTypeController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetProjectTypes()
        {
            return Ok(Enum.GetNames<ProjectType>());
        }
    }
}