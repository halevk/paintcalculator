﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaintCalculator.Api.Models;
using PriceCalculator.Data.Abstractions;

namespace PaintCalculator.Api.Controller
{
    [ApiController]
    [Route("product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet, Route("{sectorId}")]
        public async Task<IActionResult> GetProductsBySector([FromRoute] int sectorId)
        {
            var products = await _productRepository.GetProductsBySectorIds(new[] { sectorId });
            return Ok(products.Select(p => new SimpleProductViewModel
            {
                Id = p.Id,
                Name = p.Name,
                Price = p.Price,
                RedecorationCycle = p.RedecorationCycle
            }));
        }
    }
}