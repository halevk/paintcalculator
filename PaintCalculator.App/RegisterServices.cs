﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace PaintCalculator.App
{
    public static class RegisterServices
    {
        public static void AddApp(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }
    }
}