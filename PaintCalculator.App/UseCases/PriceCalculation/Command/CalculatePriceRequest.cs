﻿using MediatR;

namespace PaintCalculator.App.UseCases.PriceCalculation.Command
{
    public class CalculatePriceRequest : IRequest<CalculatePriceResponse>
    {
        public double Area { get; set; }
        public int ProductId { get; set; }
        public int SectorId { get; set; }
        public int Period { get; set; }
        public string ColorName { get; set; }
    }

    public class CalculatePriceResponse : ResponseBase
    {
        public double TotalCost { get; set; }
    }

}