﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PriceCalculator.Data.Abstractions;

namespace PaintCalculator.App.UseCases.PriceCalculation.Command
{
    public class CalculatePriceHandler : IRequestHandler<CalculatePriceRequest,CalculatePriceResponse>
    {
        private readonly ISectorProductRepository _sectorProductRepository;

        public CalculatePriceHandler(ISectorProductRepository sectorProductRepository)
        {
            _sectorProductRepository = sectorProductRepository;
        }

        public async Task<CalculatePriceResponse> Handle(CalculatePriceRequest request, CancellationToken cancellationToken)
        {
            var sectorProduct = await _sectorProductRepository.GetBySectorAndProductId(request.SectorId, request.ProductId,cancellationToken);
            if (sectorProduct == null)
                return new CalculatePriceResponse
                    { Errors = { new ResponseError { Key = "SectorProduct", Value = "There is no sector registered with this productId in the system" } } };
            var recycleCount = Math.Ceiling((double)request.Period / sectorProduct.Product.RedecorationCycle);
            var totalAmountForPeriod = Math.Round(recycleCount * request.Area * (double)sectorProduct.Product.Price * (double)sectorProduct.Sector.CostMultiplier,2);
            return new CalculatePriceResponse { TotalCost = totalAmountForPeriod };
        }
    }
}