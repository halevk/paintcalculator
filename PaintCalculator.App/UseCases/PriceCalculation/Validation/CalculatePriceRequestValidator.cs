﻿using FluentValidation;
using PaintCalculator.App.UseCases.PriceCalculation.Command;

namespace PaintCalculator.App.UseCases.PriceCalculation.Validation
{
    public class CalculatePriceRequestValidator : AbstractValidator<CalculatePriceRequest>
    {
        public CalculatePriceRequestValidator()
        {
            RuleFor(p => p.Area)
                .GreaterThan(0)
                .WithMessage("area can not be zero");
            RuleFor(p => p.Period)
                .GreaterThan(0)
                .WithMessage("period is required");
            RuleFor(p => p.ProductId)
                .GreaterThan(0)
                .WithMessage("productId is required");
            RuleFor(p => p.SectorId)
                .GreaterThan(0)
                .WithMessage("sectorId is required");
        }
    }
}