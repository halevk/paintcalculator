﻿using System.Collections.Generic;

namespace PaintCalculator.App.UseCases
{
    public abstract class ResponseBase
    {
        private readonly List<ResponseError> _errors;

        protected ResponseBase()
        {
            _errors = new List<ResponseError>();
        }

        protected ResponseBase AddError(ResponseError error)
        {
            _errors.Add(error);
            return this;
        }

        public List<ResponseError> Errors => _errors;
        public bool HasError => Errors.Count > 0;

    }

    public class ResponseError
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}