# Hack&Craft Test Case API

This app provides some endpoints which calculates total painting cost of a sector for specific product kind and period.

## Installation

### Clone the repo:

```bash
git clone https://github.com/halevk/paintcalculator.git
cd paintcalculator
```

### Working Mode :

appsettings.development.json uses following fields

"UseInMemoryDb" - decides if app uses in-memory datasource
"ConnectionString" - is used for RDBMS data source

### Installing MsSql server on local docker engine:

```bash
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=123456+-" -p 1433:1433 --name newmssql --hostname testsql -d mcr.microsoft.com/mssql/server:2019-latest
```

### Seed Initial Data

appsettings.development.json uses "SeedInitialData" flag for seeding sample data.

## Testing Endpoints

After you run the app with above settings swagger will list of all available endpoints.


